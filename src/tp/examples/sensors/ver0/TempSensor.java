package tp.examples.sensors.ver0;

import java.util.ArrayList;

public class TempSensor {

	private float t;
	private boolean running;
	private String id;

	public TempSensor(String id) {
		running = true;
		this.id = id;
	}

	public float getTemperature() {
		t = HWLib.getTemperature(id);
		return t;
	}

}
