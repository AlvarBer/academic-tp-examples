package tp.examples.sensors.ver1;


public class Main {

	public static void main(String[] args) {
		TempSensor s = new TempSensor("s");
		Heater h = new Heater("h",19,30);
		AirCondition a = new AirCondition("a",30,19);

		s.registerTempObserver(h);
		s.registerTempObserver(a);

		while ( true ) {
			s.refresh();
			sleep(5000);
		}

	}

	private static void sleep(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
		}
	}

}
