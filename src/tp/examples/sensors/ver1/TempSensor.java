package tp.examples.sensors.ver1;

import java.util.ArrayList;

public class TempSensor implements TempObservable {

	private float t;
	private ArrayList<TempObserver> ls;
	private String id;

	public TempSensor(String id) {
		ls = new ArrayList<TempObserver>();
		this.id = id;
		refresh();
	}

	public void refresh() {
		float x = HWLib.getTemperature(id);
		if (t != x) {
			t = x;
			for (TempObserver l : ls) {
				l.tempChanged(t);
			}
		}
	}

	public void registerTempObserver(TempObserver l) {
		ls.add(l);
	}
}
