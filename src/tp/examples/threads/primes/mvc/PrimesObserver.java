package tp.examples.threads.primes.mvc;

import java.math.BigInteger;

public interface PrimesObserver {
	public void onStart();
	public void onFinish(boolean halt);
	public void onGen(BigInteger n);	
}
