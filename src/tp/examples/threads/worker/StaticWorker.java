package tp.examples.threads.worker;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class StaticWorker {

	private static Queue<Runnable> tasks;

	public static void startWorker() {
		tasks = new ConcurrentLinkedQueue<Runnable>();
		new Thread() {
			public void run() {
				processTasks();
			}
		}.start();
	}

	public static void addTask(Runnable t) {
		tasks.add(t);
	}

	private static void processTasks() {
		while (!Thread.interrupted()) {
			sleepabit();
			Runnable task = tasks.poll();
			if (task != null)
				task.run();
			else
				sleepabit();
		}
	}

	private static void sleepabit() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
