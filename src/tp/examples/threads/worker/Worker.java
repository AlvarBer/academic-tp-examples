package tp.examples.threads.worker;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Worker extends Thread {

	private static Queue<Runnable> tasks;

	public Worker() {
		tasks = new ConcurrentLinkedQueue<Runnable>();
	}

	public void run() {
		processTasks();
	}

	public void addTask(Runnable t) {
		tasks.add(t);
	}

	private void processTasks() {
		while (!Thread.interrupted()) {
			Runnable task = tasks.poll();
			if (task != null)
				task.run();
			else
				sleepabit();
		}
	}

	private void sleepabit() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
