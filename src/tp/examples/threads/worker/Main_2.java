package tp.examples.threads.worker;

public class Main_2 {
	public static void main(String[] args) {
		StaticWorker.startWorker();
		
		StaticWorker.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 1");
			}
		});

		StaticWorker.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 2");
			}
		});
		
		StaticWorker.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 3");
			}
		});
		
		System.out.println("Main is done!");
	}
}
