package tp.examples.threads.worker;

public class Main_1 {
	public static void main(String[] args) {
		Worker w1 = new Worker();
		Worker w2 = new Worker();
		w1.start();
		w2.start();
		
		w2.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 1");
			}
		});

		w1.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 2");
			}
		});

		w1.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 3");
			}
		});
		
		w2.addTask( new Runnable() {
			public void run() {
				System.out.println("Task 4");
			}
		});
		
		System.out.println("Main is done!");
	}
}
