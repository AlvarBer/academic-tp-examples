package tp.examples.threads.ex5;


public class List extends Thread {

	private Cell data ;
	
	class Cell {
		int value;
		Cell next;
	}
    
	// make this method synchronized and run Test again
	//
	public void add(int x) { 
		Cell tmp = new Cell();
		tmp.value = x ;
		tmp.next = data;
		data = tmp;
	}
	
	public int length() {
		int i=0;
		Cell aux = data;
		while ( aux != null ) {
			aux = aux.next;
			i++;
		}
		return i;
			
	}


}
