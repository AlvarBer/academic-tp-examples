package tp.examples.threads.bouncingball;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class BouncingBall extends JFrame {

	private BallBoard board;
	private JLabel numBallsLabel;
	private int numBalls;
	private JButton addBall;
	private JButton pauseBalls;
	private boolean stopped;
	private Object pauseLock;

	public BouncingBall() {
		super("[=] Bouncing Ball [=]");
		initGUI();
	}

	private void initGUI() {
		JPanel mainPanel = new JPanel( new BorderLayout() );
		
		board = new BallBoard();
		mainPanel.add( board, BorderLayout.CENTER);
		
		JPanel statusBar = new JPanel();
		mainPanel.add(statusBar, BorderLayout.PAGE_END);

		numBalls = 0;
		numBallsLabel = new JLabel("Balls: "+numBalls);
		statusBar.add( numBallsLabel );

		JPanel buttonsPanel = new JPanel();
		mainPanel.add( buttonsPanel, BorderLayout.PAGE_START );

		addBall = new JButton("Add Ball");
		buttonsPanel.add(addBall);
		addBall.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final Ball b = new Ball( board.getWidth(), board.getHeight());
				board.addBall(b);
				numBalls++;
				numBallsLabel.setText("Balls: "+numBalls);
			}
		});
		
		pauseBalls = new JButton("Pause");
		buttonsPanel.add(pauseBalls);
		pauseBalls.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if ( stopped ) 
					synchronized (pauseLock) {
						stopped = false;
						pauseLock.notify();
					}
				else
					stopped = true;
					
			}
		});
		
		
		this.setContentPane(mainPanel);
		this.setPreferredSize(new Dimension(400, 400));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);		

		pauseLock = new Object();
		new Thread() {
			public void run() {
				moveBalls();
			}

		}.start();

	}

	private void moveBalls() {
		while ( true ) {
			while ( stopped ) {
				synchronized ( pauseLock ) {
					try {
						pauseLock.wait();
					} catch (InterruptedException e) {
					}
				}
			}
			SwingUtilities.invokeLater( new Runnable() {
				
				@Override
				public void run() {
					board.moveballs();					
				}
			});
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater( new Runnable() {
			
			@Override
			public void run() {
				new BouncingBall();
			}
		});
	}

}
