package tp.examples.spreadsheet;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class ObserversTable<T> {
	private Map<Integer, Map<Integer, ArrayList<T>>> obs;

	ObserversTable() {
		obs = new TreeMap<Integer, Map<Integer, ArrayList<T>>>();
	}

	void addCellObserver(int row, int col, T co) {
		Map<Integer, ArrayList<T>> x = obs.get(row);
		if (x == null) {
			x = new TreeMap<Integer, ArrayList<T>>();
			obs.put(row, x);
		}
		ArrayList<T> y = x.get(col);
		if (y == null) {
			y = new ArrayList<T>();
			x.put(col, y);
		}
		y.add(co);
	}

	void removeCellObserver(int row, int col, SpreadShellCellObserver co) {
		Map<Integer, ArrayList<T>> x = obs.get(row);
		if (x != null) {
			ArrayList<T> y = x.get(col);
			if (y != null)
				y.remove(co);
		}
	}

	Iterator<T> getCellObservers(int row, int col) {
		Map<Integer, ArrayList<T>> x = obs.get(row);
		if (x != null) {
			ArrayList<T> y = x.get(col);
			if (y != null)
				return y.iterator();
		}
		return null;
	}

}
