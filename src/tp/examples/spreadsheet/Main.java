package tp.examples.spreadsheet;

public class Main {

	static public void test1() {
		SpreadSheetImp s = new SpreadSheetImp(5, 5);
		s.setCell(1, 1, new SPCellNumber(5));
		s.setCell(2, 1, new SPCellNumber(9));
		s.setCell(3, 3, new SPCellSum(1, 1, 2, 1));
		System.out.println(s);
		s.setCell(2, 1, new SPCellNumber(17));
		System.out.println(s);
		s.setCell(1, 1, new SPCellNumber(10));
		System.out.println(s);

	}

	static public void main(String[] args) {
		SpreadSheetImp s = new SpreadSheetImp(5, 5);
		SpreadSheetInterpreter spint = new SpreadSheetInterpreter(s);
		spint.start();
	}
}
