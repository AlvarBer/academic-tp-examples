package tp.examples.spreadsheet;

import java.util.Scanner;

public class SpreadSheetInterpreter {

	SpreadSheet s;

	public SpreadSheetInterpreter(SpreadSheet s) {
		this.s = s;
	}

	public void start() {
		Scanner in = new Scanner(System.in);
		CommandName cmd = null;

		int x, y, x1, x2, y1, y2;
		SpreadSheetCell f;

		do {
			System.out.print("> ");
			String cmdStr = in.nextLine();
			String[] tokens = cmdStr.split("\\s+");
			cmd = tokenToCmd(tokens[0]);
			switch (cmd) {
			case PRINT:
				System.out.println(s);
				break;
			case ALARM:
				x = Integer.parseInt(tokens[1]);
				y = Integer.parseInt(tokens[2]);
				double z = Double.parseDouble(tokens[3]);
				s.regCellObserver(x, y, new Alarm(z));
				break;
			case SET:
				x = Integer.parseInt(tokens[1]);
				y = Integer.parseInt(tokens[2]);
				FunctionName fName = tokenToFunction(tokens[3]);
				switch (fName) {
				case SUM:
					x1 = Integer.parseInt(tokens[4]);
					y1 = Integer.parseInt(tokens[5]);
					x2 = Integer.parseInt(tokens[6]);
					y2 = Integer.parseInt(tokens[7]);
					f = new SPCellSum(x1, y1, x2, y2);
					s.setCell(x, y, f);
					break;
				case MUL:
					x1 = Integer.parseInt(tokens[4]);
					y1 = Integer.parseInt(tokens[5]);
					x2 = Integer.parseInt(tokens[6]);
					y2 = Integer.parseInt(tokens[7]);
					f = new SPCellMul(x1, y1, x2, y2);
					s.setCell(x, y, f);
					break;
				default:
					double v = Double.parseDouble(tokens[3]);
					s.setCell(x, y, new SPCellNumber(v));
				}
				break;
			case QUIT:
				System.out.println("Ciao Ciao!!");
				break;
			default:
				System.out.println("Unknow command!");
				break;
			}
		} while (cmd != CommandName.QUIT);

		in.close();
	}

	private static FunctionName tokenToFunction(String s) {
		FunctionName f = FunctionName.NONE;

		if (s.equalsIgnoreCase("SUM"))
			f = FunctionName.SUM;
		else if (s.equalsIgnoreCase("MUL"))
			f = FunctionName.MUL;

		return f;
	}

	private static CommandName tokenToCmd(String s) {
		CommandName cmd = CommandName.NONE;

		if (s.equalsIgnoreCase("SET"))
			cmd = CommandName.SET;
		else if (s.equalsIgnoreCase("PRINT"))
			cmd = CommandName.PRINT;
		else if (s.equalsIgnoreCase("ALARM"))
			cmd = CommandName.ALARM;
		else if (s.equalsIgnoreCase("QUIT"))
			cmd = CommandName.QUIT;

		return cmd;
	}
}
