package tp.examples.spreadsheet;

import java.util.Iterator;

public class SpreadSheetImp implements SpreadSheet {
	private final static int defaultRowsValue = 4;
	private final static int defaultColsValue = 4;
	private int rows;
	private int cols;
	private SpreadSheetCell[][] table;
	private ObserversTable<SpreadShellCellObserver> obs;

	public SpreadSheetImp() {
		this(defaultRowsValue, defaultColsValue);
	}

	public SpreadSheetImp(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		this.table = new SpreadSheetCell[rows][cols];
		obs = new ObserversTable<SpreadShellCellObserver>();

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				this.table[i][j] = new SPCellNumber();
	}

	@Override
	public SpreadSheetCell getCell(int row, int col) {
		return table[row][col];
	}

	@Override
	public double getCellValue(int row, int col) {
		return table[row][col].getNumericValue();
	}

	@Override
	public void setCell(final int row, final int col, final SpreadSheetCell c) {
		table[row][col].removed();
		table[row][col] = c;
		c.installed(new SPConnection() {

			@Override
			public double getValue(int x, int y) {
				return table[x][y].getNumericValue();
			}

			@Override
			public void changedValue() {
				notifyChanges(row, col);
			}

			@Override
			public void register(int x, int y) {
				regCellObserver(x, y, c);
			}

			@Override
			public void unregister(int x, int y) {
				unregCellObserver(x, y, c);
			}
		});
		notifyChanges(row, col);
	}

	private void notifyChanges(int row, int col) {
		Iterator<SpreadShellCellObserver> x = obs.getCellObservers(row, col);
		if (x != null) {
			double v = table[row][col].getNumericValue();
			while (x.hasNext())
				x.next().valueChanged(row, col, v);

		}
	}

	public int getWidth() {
		return cols;
	}
	
	public int getHeight() {
		return rows;
	}

	public void regCellObserver(int row, int col, SpreadShellCellObserver co) {
		obs.addCellObserver(row, col, co);
	}

	public void unregCellObserver(int row, int col, SpreadShellCellObserver co) {
		obs.removeCellObserver(row, col, co);
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++)
				s = s + "| " + table[i][j];
			s = s + "|" + System.getProperty("line.separator");
		}
		return s;
	}

}
