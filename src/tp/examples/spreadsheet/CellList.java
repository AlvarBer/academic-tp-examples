package tp.examples.spreadsheet;

import java.util.ArrayList;

// we define this class because it is not allowed to create
// an array of element of type ArryList<CellObserver>
//
public class CellList extends ArrayList<SpreadSheetCell> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
