package tp.examples.spreadsheet;

import tp.examples.spreadsheet.SpreadSheet.SPConnection;

abstract class SpreadSheetCell implements SpreadShellCellObserver {
	protected double numericValue;
	protected String strValue;

	protected SpreadSheetCell() {
		numericValue = 0;
		strValue = "";
	}

	public double getNumericValue() {
		return numericValue;
	};

	public String getStrValue() {
		return strValue;
	}

	abstract public void installed(SPConnection com);

	abstract public void removed();

	public String toString() {
		return Double.toString(numericValue);
	}
}
