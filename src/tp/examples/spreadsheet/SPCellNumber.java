package tp.examples.spreadsheet;

import tp.examples.spreadsheet.SpreadSheet.SPConnection;

public class SPCellNumber extends SpreadSheetCell {

	public SPCellNumber() {
		numericValue = 0;
		strValue = " ";
	}

	public SPCellNumber(double v) {
		numericValue = v;
		strValue = Double.toString(v);
	}

	public SPCellNumber(String strV) {
		numericValue = Double.parseDouble(strV);
		strValue = strV;
	}

	@Override
	public void installed(SPConnection com) {
	}

	@Override
	public void removed() {
	}

	public String toString() {
		return strValue;
	}

	@Override
	public void valueChanged(int x, int y, double v) {
	}
}
