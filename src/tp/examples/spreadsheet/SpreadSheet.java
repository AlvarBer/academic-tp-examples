package tp.examples.spreadsheet;

public interface SpreadSheet {
	public double getCellValue(int row, int col);

	public SpreadSheetCell getCell(int row, int col);

	public void setCell(int row, int col, SpreadSheetCell c);
	public int getWidth();
	public int getHeight();
	public void regCellObserver(int row, int col, SpreadShellCellObserver co);

	public void unregCellObserver(int row, int col, SpreadShellCellObserver co);

	interface SPConnection {
		public double getValue(int x, int y);

		public void changedValue();

		public void register(int x, int y);

		public void unregister(int x, int y);
	}

}
