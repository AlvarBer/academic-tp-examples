package tp.examples.spreadsheet;

import tp.examples.spreadsheet.SpreadSheet.SPConnection;

public class SPCellMul extends SpreadSheetCell {

	int x1, y1, x2, y2;
	SPConnection s;

	public SPCellMul(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		numericValue = 0;
		strValue = "=mul(" + x1 + "," + y1 + "," + x2 + "," + y2 + ")";
	}

	@Override
	public void installed(SPConnection s) {
		this.s = s;
		updateValue();
		s.register(x1, y1);
		s.register(x2, y2);
	}

	@Override
	public void removed() {

	}

	@Override
	public void valueChanged(int x, int y, double v) {
		updateValue();
	}

	private void updateValue() {
		double v = s.getValue(x1, y1) * s.getValue(x2, y2);
		if (v != numericValue) {
			numericValue = v;
			s.changedValue();
		}
	}

}
