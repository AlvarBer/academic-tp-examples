package tp.examples.spreadsheet;

public interface SpreadShellCellObserver {
	abstract public void valueChanged(int x, int y, double v);

}
