package tp.examples.swing.mvc.views.log;

import java.io.FileWriter;
import java.io.IOException;

import tp.examples.swing.mvc.logic.ImmutableBoard;
import tp.examples.swing.mvc.logic.Observable;
import tp.examples.swing.mvc.logic.SlidePuzzleObserver;

public class LogView implements SlidePuzzleObserver {

	private String fname;

	public LogView(Observable<SlidePuzzleObserver> game, String fname) {
		this.fname = fname;
		game.addObserver(this);
	}

	private void log(String s) {
		try {
			FileWriter fw = new FileWriter(fname, true);
			fw.write(s);
			fw.write("\n");
			fw.close();
		} catch (IOException e) {
		}
	}

	@Override
	public void onReset(ImmutableBoard board) {
		log(board.toString());
	}

	@Override
	public void onMove(ImmutableBoard board, int srcRow, int srcCol,
			int trgtRow, int trgtCol) {
		log(board.toString());
		log("Moved (" + srcRow + "," + srcCol + ") to (" + trgtRow
				+ "," + trgtCol + ")");
	}

	@Override
	public void onError(String msg) {
	}

	@Override
	public void onGameOver(ImmutableBoard board) {
		log("Game over!");
	}
}
