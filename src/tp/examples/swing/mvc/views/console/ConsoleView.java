package tp.examples.swing.mvc.views.console;

import tp.examples.swing.mvc.logic.ImmutableBoard;
import tp.examples.swing.mvc.logic.Observable;
import tp.examples.swing.mvc.logic.SlidePuzzleObserver;

public class ConsoleView implements SlidePuzzleObserver {

	public ConsoleView(Observable<SlidePuzzleObserver> game) {
		game.addObserver(this);
	}

	@Override
	public void onReset(ImmutableBoard board) {
		System.out.println(board);
	}

	@Override
	public void onMove(ImmutableBoard board, int srcRow, int srcCol, int trgtRow, int trgtCol) {
		System.out.println(board);
	}

	@Override
	public void onError(String msg) {
		System.err.println(msg);
	}

	@Override
	public void onGameOver(ImmutableBoard board) {
		System.out.println("Game over!");
	}

}
