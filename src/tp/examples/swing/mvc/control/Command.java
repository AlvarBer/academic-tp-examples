package tp.examples.swing.mvc.control;

import tp.examples.swing.mvc.logic.InvalidMove;

public interface Command {
	void execute(Controller c) throws InvalidMove;
	String helpText();
	Command parse(String []lineWords);	
}
