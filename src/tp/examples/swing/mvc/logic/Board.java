package tp.examples.swing.mvc.logic;

public class Board implements ImmutableBoard {
	private int[][] board;
	private int rows;
	private int cols;

	public Board(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		board = new int[rows][cols];
	}

	public boolean setPostion(int row, int col, int v) {
		if ( row >= 1 && row <= rows && col >= 1 && col <= cols ) {
			board[row-1][col-1] = v;
			return true;
		} else
			return false;
	}

	@Override
	public int getPostion(int row, int col) {
		if ( row >= 1 && row <= rows && col >= 1 && col <= cols ) {
			return board[row-1][col-1];
		} else {
			return -1;
		}
	}
	
	@Override
	public int getWidth() {
		return cols;
	}

	@Override
	public int getHeight() {
		return rows;
	}


	

	public String toString() {
		StringBuilder render = new StringBuilder();

		for (int i = 0; i < rows; i++) {
			render.append("|");
			for (int j = 0; j < cols; j++) {
				if (board[i][j] == 0 )
					render.append("       |");
				else						
					render.append(String.format(" %5d |",board[i][j]));
			}
			render.append("\n");
		}
		return render.toString();
	}


}
