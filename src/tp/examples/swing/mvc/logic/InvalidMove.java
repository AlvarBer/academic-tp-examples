package tp.examples.swing.mvc.logic;

@SuppressWarnings("serial")
public class InvalidMove extends Exception {

	public InvalidMove(String msg) {
		super(msg);
	}

}
