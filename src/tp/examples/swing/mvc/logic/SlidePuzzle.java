package tp.examples.swing.mvc.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class SlidePuzzle implements Observable<SlidePuzzleObserver>{

	private Board board;
	private boolean finished;
	private int rows;
	private int cols;
	private Collection<SlidePuzzleObserver> obs;

	public SlidePuzzle() {
		this(4,4);
	}
	
	public SlidePuzzle(int rows, int cols) {
		this.obs = new ArrayList<SlidePuzzleObserver>();
		reset(rows,cols);
	}

	public void move(int row, int col) throws InvalidMove {
		
		if ( finished ) {
			for(SlidePuzzleObserver o : obs) {
				o.onError("Game is finshed already!");
			}
			throw new InvalidMove("Game is finshed already!");
		}
		
		int i = -1;
		int j = -1;
		
		if ( board.getPostion(row-1, col) == 0 ) {
			i = row - 1;
			j = col;
		} else if ( board.getPostion(row+1, col) == 0 ) {
			i = row + 1;
			j = col;
		} else if ( board.getPostion(row, col-1) == 0 ) {
			i = row;
			j = col - 1;
		} else if ( board.getPostion(row, col+1) == 0 ) {
			i = row;
			j = col + 1;
		}
		
		if ( i == -1 || j == -1 ) {
			for(SlidePuzzleObserver o : obs) {
				o.onError("Invalid move "+"("+row+","+col+")");
			}
			throw new InvalidMove("Invalid move "+"("+row+","+col+")");
		}
		
		// swap
		int v = board.getPostion(row, col);
		board.setPostion(row, col, board.getPostion(i, j));
		board.setPostion(i,j,v);

		for(SlidePuzzleObserver o : obs) {
			o.onMove(board, row, col, i, j);
		}

		if ( checkIfFinished() ) {
			finished = true;
			for(SlidePuzzleObserver o : obs) {
				o.onGameOver(board);
			}
		}
	}

	private boolean checkIfFinished() {
		int maxNum  = rows * cols;
		int currNum = 1;
		
		for(int i=1; i<=rows; i++)
			for(int j=1; j<=cols; j++) {
				if ( board.getPostion(i, j) != currNum )
					return false;
				currNum = (currNum+1) % maxNum;
			}
		return true;
	}

	public boolean isFinished() {				
		return finished;
	}

	public int getRows() {
		return rows;
	}
	
	public int getColss() {
		return cols;
	}

	public void reset(int rows, int cols) {
		rows = Math.max(rows, 2);
		cols = Math.max(cols, 2);
		this.rows = rows;
		this.cols = cols;
		int maxNum = rows * cols;
		board = new Board(rows, cols);
		finished = false;

		Random r = new Random();
		boolean[] usedNums = new boolean[maxNum];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++) {
				int k = r.nextInt(maxNum);
				while (usedNums[k])
					k = (k + 1) % maxNum;
				usedNums[k] = true;
				board.setPostion(i+1, j+1, k);
			}
		
		for(SlidePuzzleObserver o : obs) {
			o.onReset(board);
		}
	}

	public String toString() {
		return board.toString();
	}

	@Override
	public void addObserver(SlidePuzzleObserver o) {
		obs.add(o);
		o.onReset(board);
	}

	@Override
	public void removeObserver(SlidePuzzleObserver o) {
		obs.remove(o);
	}
}
