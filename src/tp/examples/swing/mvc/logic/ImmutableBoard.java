package tp.examples.swing.mvc.logic;

public interface ImmutableBoard {
	public int getPostion(int row, int col);
	public int getWidth();
	public int getHeight();
}
