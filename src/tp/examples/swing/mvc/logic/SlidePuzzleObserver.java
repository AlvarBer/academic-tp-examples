package tp.examples.swing.mvc.logic;

public interface SlidePuzzleObserver {
	public void onReset(ImmutableBoard board);
	public void onMove(ImmutableBoard board, int srcRow, int srcCol, int trgtRow, int trgtCol);
	public void onError(String msg);
	public void onGameOver(ImmutableBoard board);
}
