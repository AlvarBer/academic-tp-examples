package tp.exercises;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Slave {
	private Executor exec;

	public Slave() {
		exec = Executors.newCachedThreadPool();
	}

	public void execute(Runnable t) {
		exec.execute(t);
	}

	public void executeAndWait(final Runnable t) {
		Runnable tWithNotify = new Runnable() {
			@Override
			public void run() {
				t.run();
				synchronized (this) {
					this.notify();
				}
			}
		};

		synchronized (tWithNotify) {
			exec.execute(tWithNotify);

			try {
				tWithNotify.wait();
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		Runnable myT = new Runnable() {
			@Override
			public void run() {
				for (int j = 0; j < 100; ++j) {
					System.out.println("Runnable number: " + j);
				}
			}
		};

		Slave t = new Slave();

		//t.execute(myT);
		t.executeAndWait(myT);
		for (int j = 0; j < 100; ++j) {
			System.out.println("Main number: " + j);
		}


	}

}