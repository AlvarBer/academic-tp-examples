package tp.exercises;

public class PingPong {

	public static void main(String[] args) {
		final Object lock = new Object();

		Runnable pingThread = new Runnable() {
			@Override
			public void run() {
				while(true) {
					synchronized (lock) {
						System.out.println("Ping");
						lock.notify();
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					try {

						Thread.currentThread().sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		Runnable pongThread = new Runnable() {
			@Override
			public void run() {
				while (true) {
					synchronized (lock) {
						System.out.println("Pong");
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					try {

						Thread.currentThread().sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		Thread t1 = new Thread(pingThread);

		Thread t2 = new Thread(pongThread);
		t1.run();
		t2.run();
	}
}
